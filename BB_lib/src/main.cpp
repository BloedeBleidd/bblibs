
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <algorithm>
#include <functional>
#include "BB_lib/bb_lib.h"

using namespace BB;

typedef int cType;
std::function<bool ( const cType &, const cType & )> desc = ([]( cType a, cType b ){ return a < b; });
std::function<bool ( const cType &, const cType & )> asc = ([]( cType a, cType b ){ return a > b; });

template <typename T>
void draw( const T *tab, size_t n )
{
	while(--n)
		std::cout << *tab++ << ", ";
	std::cout << *tab;
}
/*
int main(void)
{
	char buf[20];
	Crc crc( Crc::CRC16_USB );
	std::string s;

	std::getline( std::cin, s );
	uint32_t r = crc.calculate( s.c_str(), buf );
	std::cout << "> CRC_USB <\n" << "crc: " << r << " , hex: " << buf << "\n\r";

	cType tab[] = { 99999,-100,234,65,234,57,67,45,324,46,-352325,324432,-5,685,97,98788 };

	Sorting::bubble( tab, sizeof(tab)/sizeof(*tab), asc );

	draw( tab, sizeof(tab)/sizeof(*tab) );

	getch();
	getch();

	return 0;
}
*/

int main()
{
	typedef string myT;
	Fifo<myT> a;

	a.enqueue( "dsdsgdsds" );
	a.enqueue( "2" );
	a.enqueue( "trzy" );

	myT x;
	a.dequeue( x );
	std::cout << x << "\n\r";
	a.dequeue( x );
	std::cout << x << "\n\r";
	a.dequeue( x );
	std::cout << x << "\n\r";
	a.dequeue( x );
	std::cout << x << "\n\r";
}
