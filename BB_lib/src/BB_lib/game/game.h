/*
 * game.h
 *
 *  Created on: Jul 28, 2018
 *      Author: BloedeBleidd
 */

#ifndef GAME_H_
#define GAME_H_


template <typename Time> class Game
{
private:
	typedef bool (*UpdateFunc) ( Time );
	typedef void (*RenderFunc) ( void );
	typedef void (*SleepFunc) ( Time );
	typedef Time (*TimeFunc) ( void );

	static const Time SECOND = 1000;
	UpdateFunc update;
	RenderFunc render;
	SleepFunc sleep;
	TimeFunc time;
	Time updateDelay, renderDelay;

public:
	Game( UpdateFunc u, RenderFunc r, SleepFunc s, TimeFunc t, Time gameSpeed, Time fpsLimit ) : update(u), render(r), sleep(s), time(t), updateDelay(SECOND/gameSpeed), renderDelay(SECOND/fpsLimit) {}
	void run();
};

template <typename Time>
void Game<Time>::run()
{
	Time nextUpdate = time();
	Time nextRender = nextUpdate;

	for(;;)
	{
		if( time() >= nextUpdate )
		{
			if( !update( time() ) )
				return;
			nextUpdate += updateDelay;
		}

		if( time() >= nextRender )
		{
			render();
			nextRender += renderDelay;
		}

		if( nextUpdate < nextRender )
			sleep( nextUpdate-time() );
		else
			sleep( nextRender-time() );
	}
}


#endif /* GAME_H_ */
