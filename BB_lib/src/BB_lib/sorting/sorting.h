/*
 * sorting.h
 *
 *  Created on: 4 gru 2017
 *      Author: Piter
 */

#ifndef SORTING_H_
#define SORTING_H_

#include "bubble.h"
#include "heap.h"
#include "insertion.h"
#include "merge.h"
#include "quick.h"
#include "selection.h"

//std::function<bool ( const Type &, const Type & )> descending = ([]( Type a, Type b ){ return a < b; });

#endif /* SORTING_H_ */
