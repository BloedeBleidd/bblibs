/*
 * selection.h
 *
 *  Created on: 4 gru 2017
 *      Author: Piter
 */

#ifndef SORTING_SELECTION_H_
#define SORTING_SELECTION_H_

#include <functional>

namespace BB
{

namespace Sorting
{
	template <typename T>
	void selection( T *array, size_t first, size_t last, std::function<bool(const T &,const T &)> comparator )
	{
		for ( size_t i=first; i<=last; i++ )
		{
			size_t tmp = i;
			for ( size_t j=i+1; j<=last; j++ )
				if ( comparator( array[tmp], array[j] ) )
					tmp = j;
			std::swap( array[i], array[tmp] );
		}
	}

	template <typename T>
	void selection( T *array, size_t size, std::function<bool(const T &,const T &)> comparator )
	{
		selection( array, 0, size-1, comparator );
	}
}

}

#endif /* SORTING_SELECTION_H_ */
