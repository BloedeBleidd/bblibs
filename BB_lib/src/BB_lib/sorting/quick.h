/*
 * quick.h
 *
 *  Created on: 4 gru 2017
 *      Author: Piter
 */

#ifndef QUICK_H_
#define QUICK_H_

#include <cstdlib>
#include <functional>

namespace BB
{

namespace Sorting
{
	template <typename T>
	void quickSort( T *array, size_t l, size_t r, std::function<bool(const T &,const T &)> comparator )
	{
		size_t i = l,j = r;
		T tmp = array[ div( l+r, 2 ).quot ];

		do{
			for( ; comparator( tmp, array[i] ); i++ ) {}
			for( ; comparator( array[j], tmp ); j-- ) {}

			if ( i <= j )
			{
				std::swap( array[i], array[j] );
				i++; j--;
			}
		}
		while( i <= j );

		if( l < j )
			quickSort( array, l, j, comparator );
		if( i < r )
			quickSort( array, i, r, comparator );
	}

	template <typename T>
	void quick( T *array, size_t size, std::function<bool(const T &,const T &)> comparator )
	{
		quickSort( array, 0, size-1, comparator );
	}

	template <typename T>
	void quick( T *array, size_t first, size_t last, std::function<bool(const T &,const T &)> comparator )
	{
		quickSort( array, first, last, comparator );
	}
}

}

#endif /* QUICK_H_ */
