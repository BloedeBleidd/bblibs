/*
 * insertion.h
 *
 *  Created on: 4 gru 2017
 *      Author: Piter
 */

#ifndef INSERTION_H_
#define INSERTION_H_

namespace BB
{

namespace Sorting
{
	template <typename T>
	void insertionSort( T *array, size_t l, size_t r, std::function<bool(const T &,const T &)> comparator )
	{
		for( int i=l+1; i<(int)r; i++ )
		{
			T tmp = array[i];
			int j;

			for( j=i-1; j>=(int)l && comparator( array[j], tmp ); j-- )
				array[j+1] = array[j];

			array[j+1] = tmp;
		}
	}

	template <typename T>
	void insertion( T *array, size_t size, std::function<bool(const T &,const T &)> comparator )
	{
		insertionSort( array, 0, size, comparator );
	}

	template <typename T>
	void insertion( T *array, size_t first, size_t last, std::function<bool(const T &,const T &)> comparator )
	{
		insertionSort( array, first, last+1, comparator );
	}
}

}

#endif /* INSERTION_H_ */
