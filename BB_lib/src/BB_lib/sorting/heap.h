/*
 * heap.h
 *
 *  Created on: 4 gru 2017
 *      Author: Piter
 */

#ifndef SORTING_HEAP_H_
#define SORTING_HEAP_H_

#include <algorithm>
#include <functional>

namespace BB
{

namespace Sorting
{
	template<typename T>
	void checkRoot( T *array, size_t root, size_t size, std::function<bool(const T &,const T &)> comparator )
	{
		size_t left = 2*root, right = 2*root + 1;

		if( left < size && comparator( array[left], array[root] ) )
		{
			std::swap( array[root], array[left] );
			checkRoot( array, left, size, comparator );
		}

		if( right < size && comparator( array[right], array[root] ) )
		{
			std::swap( array[root], array[right] );
			checkRoot( array, right, size, comparator );
		}
	}

	template<typename T>
	void heap( T *array, size_t size, std::function<bool(const T &,const T &)> comparator )
	{
		for( int i=size/2; i>=0; i-- )					// build heap
			checkRoot( array, i, size, comparator );

		for( ; size>1; size-- )							// sort heap
		{
			std::swap( array[0], array[size-1] );
			checkRoot( array, 0, size-1, comparator );
		}
	}

	template<typename T>
	void heap( T *array, size_t first, size_t last, std::function<bool(const T &,const T &)> comparator )
	{
		heap( array+first, last-first, comparator );
	}
}

}

#endif /* SORTING_HEAP_H_ */
