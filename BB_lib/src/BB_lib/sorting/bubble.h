/*
 * bubble.h
 *
 *  Created on: 4 gru 2017
 *      Author: Piter
 */

#ifndef BUBBLE_H_
#define BUBBLE_H_

#include <functional>

namespace BB
{

namespace Sorting
{
	template<typename T>
	void bubble( T *array, size_t first, size_t last, std::function<bool(const T &,const T &)> comparator )
	{
		size_t amount = last-first;
		for ( size_t i=first; i<amount-1; ++i )
			for ( size_t j=amount-1; j>i; --j )
				if ( comparator( array[j-1], array[j] ) )
					std::swap( array[j-1], array[j] );
	}

	template<typename T>
	void bubble( T *array, size_t size, std::function<bool(const T &,const T &)> comparator )
	{
		bubble( array, 0, size-1, comparator );
	}
}

}

#endif /* BUBBLE_H_ */
