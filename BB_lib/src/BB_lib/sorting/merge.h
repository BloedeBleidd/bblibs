
/*
 * merge.h
 *
 *  Created on: 4 gru 2017
 *      Author: Piter
 */

#ifndef MERGE_H_
#define MERGE_H_

#include <functional>

namespace BB
{

namespace Sorting
{
	template<typename T>
	void merging( T *arr, size_t l, size_t m, size_t r, std::function<bool(const T &,const T &)> comparator )
	{
		size_t i,j,k;
		size_t n1 = 1+m-l, n2 = r-m;
		T left[n1], right[n2];

		for( i=0; i<n1; i++ )
			left[i] = arr[l+i+1];

		for( i=0; i<n2; i++ )
			right[i] = arr[m+l+i+1];

		for( i=0, j=0, k=0; i<n1 && j<n2; k++ )
			if( comparator( left[i], right[j] ) )
				arr[k] = left[i++];
			else
				arr[k] = right[j++];

		for( ; i<n1; i++, k++ )
			arr[k] = left[i];

		for( ; j<n2; j++, k++ )
			arr[k] = left[j];
	}

	template<typename T>
	void merge( T *array, size_t first, size_t last, std::function<bool(const T &,const T &)> comparator )
	{
		if( first < last || last <= 0 || first < 0 )
			return;

		size_t m = first+(last-first)/2;
		merge( array, first, m, comparator );
		merge( array, m+first, last, comparator );
		merging( array, first, m, last, comparator );
	}

	template<typename T>
	void merge( T *array, size_t size, std::function<bool(const T &,const T &)> comparator )
	{
		merge( array, 0, size-1, comparator );
	}
}

}

#endif /* MERGE_H_ */
