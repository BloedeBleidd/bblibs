/*
 * dft.h
 *
 *  Created on: Mar 13, 2018
 *      Author: Y520
 */

#ifndef BB_LIB_FOURIER_DFT_H_
#define BB_LIB_FOURIER_DFT_H_

#include <cstdlib>
#include <cmath>
#include <vector>
#include <array>
#include <complex>

namespace BB
{
namespace Fourier
{
	template<typename T>
	void dft( const std::complex<T> *input, std::complex<T> *output, size_t len )
	{
		for( size_t i=0; i<len; i++ )
		{
			std::complex<T> sum( 0.0, 0.0 );
			for(size_t j=0; j<len; j++ )
				sum += input[j] * exp( -2.0*M_PI * j * i / len );
			output[i] = sum;
		}
	}

	template<typename T>
	void dftInverse( const std::complex<T> *input, std::complex<T> *output, size_t len )
	{
	    for( size_t i=0; i<len; i++ )
	    {
	    	std::complex<T> sum( 0.0, 0.0 );
	    	for( size_t j=0; j<len; j++ )
	        {
	    		T angle = 2.0*M_PI * j * i / len;
	            sum += { input[j].real()*cos( angle ) - input[j].imag()*sin( angle ), input[j].real()*sin( angle ) + input[j].imag()*cos( angle ) };
	        }
	    	output[i] = sum;
	    }
	}

	template<typename T>
	void dft( const std::vector<std::complex<T>> &input, std::vector<std::complex<T>> &output )
	{
		if( input.size() != output.size() )
			output.resize( input.size(), 0 );

		for( size_t i=0; i<input.size(); i++ )
			{
				std::complex<T> sum( 0.0, 0.0 );
				for (size_t j=0; j<input.size(); j++ )
					sum += input[j] * exp( -2.0*M_PI * j * i / input.size() );
				output[i] = sum;
			}
	}

	template<typename T>
	void dftInverse( const std::vector<std::complex<T>> &input, std::vector<std::complex<T>> &output )
	{
		if( input.size() != output.size() )
			output.resize( input.size(), 0 );

		for( size_t i=0; i<input.size(); i++ )
		{
			std::complex<T> sum( 0.0, 0.0 );
			for( size_t j=0; j<input.size(); j++ )
			{
				T angle = 2.0*M_PI * j * i / input.size();
				sum += { input[j].real()*cos( angle ) - input[j].imag()*sin( angle ), input[j].real()*sin( angle ) + input[j].imag()*cos( angle ) };
			}
			output[i] = sum;
		}
	}

	template<typename T, std::size_t len>
	void dft( const std::array<std::complex<T>, len> &input, std::array<std::complex<T>, len> &output )
	{
		if( input.size() != output.size() )
			output.resize( input.size(), 0 );

		for( size_t i=0; i<input.size(); i++ )
		{
			std::complex<T> sum( 0.0, 0.0 );
			for (size_t j=0; j<input.size(); j++ )
				sum += input[j] * exp( -2.0*M_PI * j * i / input.size() );
			output[i] = sum;
		}
	}

	template<typename T, std::size_t len>
	void dftInverse( const std::array<std::complex<T>, len> &input, std::array<std::complex<T>, len> &output )
	{
		if( input.size() != output.size() )
			output.resize( input.size(), 0 );

		for( size_t i=0; i<input.size(); i++ )
		{
			std::complex<T> sum( 0.0, 0.0 );
			for( size_t j=0; j<input.size(); j++ )
			{
				T angle = 2.0*M_PI * j * i / input.size();
				sum += { input[j].real()*cos( angle ) - input[j].imag()*sin( angle ), input[j].real()*sin( angle ) + input[j].imag()*cos( angle ) };
			}
			output[i] = sum;
		}
	}
}
}

#endif /* BB_LIB_FOURIER_DFT_H_ */
