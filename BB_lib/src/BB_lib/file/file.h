/*
 * file.h
 *
 *  Created on: Sep 21, 2018
 *      Author: BloedeBleidd
 */

#ifndef BB_LIB_FILE_FILE_H_
#define BB_LIB_FILE_FILE_H_

#include <cstdlib>
#include <cstdint>

class BaseFile
{
public:
	struct Attributes { bool readOnly:1; bool hidden:1; bool archive:1; bool system:1; };
	enum Type { File, Folder };
	typedef uint32_t Time;

	static const Attributes NO_ATTRIBUTES;
	static const char *NONE;

private:
	char *name;
	Type type;
	Time time;
	Attributes attr;

public:
	BaseFile( const char *, Type, Time, Attributes );
	BaseFile( const BaseFile & );
	virtual ~BaseFile() = 0;

	virtual BaseFile & operator=( const BaseFile & );
	virtual bool operator==( const BaseFile & ) const;
	char operator[]( size_t num ) const			{ return name[num]; }

	const char * getName() const				{ return name; }
	Type getType() const						{ return type; }
	Time getTime() const						{ return time; }
	Attributes getAttributes() const			{ return attr; }
	bool isReadOnly() const						{ return attr.readOnly; }
	bool isHidden() const						{ return attr.readOnly; }
	bool isArchive() const						{ return attr.archive; }
	bool isSystem() const						{ return attr.system; }

	void setName( const char * );
	void setType( Type t )						{ type = t; }
	void setTime( Time t )						{ time = t; }
	void setAttributes( Attributes a )			{ attr = a; }
	void setAttributeReadOnly( bool ro = true )	{ attr.readOnly = ro; }
	void setAttributeHidden( bool h = true )	{ attr.hidden = h; }
	void setAttributeArchive( bool a = true )	{ attr.archive = a; }
	void setAttributeSystem( bool s = true )	{ attr.system = s; }
};

class File : public BaseFile
{
private:
	size_t size;

public:
	File( const char * = NONE, Time = 0, Attributes = NO_ATTRIBUTES, size_t = 0 );
	File( const File & );
	virtual ~File();

	virtual File & operator=( const File & );
	virtual bool operator==( const File & ) const;

	size_t getSize() const						{ return size; }
	const char * getFormat() const;

	void setSize( size_t s )					{ size = s; }
};

class Folder : public BaseFile
{
private:

public:
	Folder( const char * = NONE );
	Folder( const Folder & );
	virtual ~Folder();

	virtual Folder & operator=( const Folder & );
	virtual bool operator==( const Folder & ) const;
};

class Directory
{
private:
	static const char DEFAULT_SEPARATOR = '/';
	char *path;
	char separator[2];

public:
	Directory( const char * = "", char = DEFAULT_SEPARATOR );
	Directory( const Directory & );
	Directory( const BaseFile &, char = DEFAULT_SEPARATOR );
	~Directory();

	bool operator==( const Directory & ) const;
	char operator[]( size_t num ) const	{ return path[num]; }
	Directory & operator=( const Directory & );
	Directory & operator=( const char * );
	Directory & operator=( const BaseFile & );
	Directory & operator+=( const Directory & );
	Directory & operator+=( const char * );
	Directory & operator+=( const BaseFile & );
	Directory operator+( const Directory & ) const;
	Directory operator+( const char * ) const;
	Directory operator+( const BaseFile & ) const;

	friend Directory operator+( const char *, const Directory & );
	friend Directory operator+( const char *, const BaseFile & );
	friend Directory operator+( const Folder &, const BaseFile & );

	Directory & operator-=( size_t );
	Directory & operator--();
	friend Directory & operator--( Directory & );

	bool back( size_t = 1 );
	void add( const char * );
	void add( const Directory & );
	void add( const BaseFile & );

	const char * get() const				{ return path; };
	char getSeparator() const					{ return separator[0]; }

	void set( const char * );
	void set( const Directory & );
	void set( const BaseFile & );
	void setSeparator( char s )					{ separator[0] = s; }

	void reset();
};




#endif /* BB_LIB_FILE_FILE_H_ */
