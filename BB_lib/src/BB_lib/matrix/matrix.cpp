#include "matrix.h"

template<typename T>
Matrix<T>::Matrix( size_t r, size_t c, const T &initVal )
: rows(r), columns(c)
{
	data.resize( r );

	for( size_t i=0; i<r; i++ )
		data[i].resize( c, initVal );
}

template<typename T>
Matrix<T>::Matrix( const Matrix<T> &matrix )
: rows( matrix.rows ), columns( matrix.columns ), data( matrix.data )
{

}

template<typename T>
Matrix<T>::~Matrix()
{

}

template<typename T>
Matrix<T> Matrix<T>::transpose() const
{
	Matrix tmp( rows, columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = data[j][i];

	return tmp;
}

template<typename T>
Matrix<T> Matrix<T>::cofactor() const
{
	Matrix<T> tmp( this );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = 0;
	return tmp;
}

template<typename T>
Matrix<T> Matrix<T>::inverse() const
{
	Matrix<T> tmp( this );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = 0;
	return tmp;
}

template<typename T>
std::vector<T> Matrix<T>::diagonal() const
{
	std::vector<T> tmp( rows, 0 );

	for( size_t i=0; i<rows; i++ )
		tmp[i] = data[i][i];

	return tmp;
}

template<typename T>
T Matrix<T>::determinant() const
{
	if( 1 == rows && 1 == columns )
		return data[0][0];
	else if( 2 == rows && 2 == columns )
		return data[0][0] * data[1][1] - data[0][1] * data[1][0];
	else if( columns == rows )
		return calculateDet( data, rows );
	else
		return 0;
}

template<typename T>
size_t Matrix<T>::rank() const
{
	size_t tmp = 0;

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp++;

	return tmp;
}

template<typename T>
T Matrix<T>::getElement( size_t r, size_t c ) const
{
	return data[r][c];
}

template<typename T>
void Matrix<T>::setElement( size_t r, size_t c, const T &value )
{
	data[r][c] = value;
}

template<typename T>
size_t Matrix<T>::getRows() const
{
	return rows;
}

template<typename T>
size_t Matrix<T>::getColumns() const
{
	return columns;
}

template<typename T>
T & Matrix<T>::operator()( size_t r, size_t c )
{
	return data[r][c];
}

template<typename T>
const T& Matrix<T>::operator()( size_t r, size_t c ) const
{
	return data[r][c];
}

template<typename T>
Matrix<T> & Matrix<T>::operator=( const Matrix<T> &matrix )
{
	if( &matrix == this )
		return *this;

	rows = matrix.rows;
	columns = matrix.columns;
	data.resize( rows );

	for( size_t i=0; i<rows; i++ )
		data[i].resize( columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			data[i][j] = matrix.data[i][j];

	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator+( const Matrix<T> &matrix )
{
	Matrix<T> tmp( rows, columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = data[i][j] + matrix.data[i][j];

	return tmp;
}

template<typename T>
Matrix<T> & Matrix<T>::operator+=( const Matrix<T> &matrix )
{
	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			data[i][j] += matrix.data[i][j];

	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator-( const Matrix<T> &matrix )
{
	Matrix<T> tmp( rows, columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = data[i][j] - matrix.data[i][j];

	return tmp;
}

template<typename T>
Matrix<T> & Matrix<T>::operator-=( const Matrix<T> &matrix )
{
	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			data[i][j] -= matrix.data[i][j];

	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator*( const Matrix<T> &matrix )
{
	Matrix tmp( rows, matrix.getColumns() );

	for( size_t i=0; i<tmp.getRows(); i++ )
		for( size_t j=0; j<tmp.getColumns(); j++ )
			for( size_t k=0; k<columns; k++ )
				tmp.data[i][j] += data[i][k] * matrix.data[k][j];

	return tmp;
}

template<typename T>
Matrix<T> & Matrix<T>::operator*=( const Matrix<T> &matrix )
{
	Matrix tmp = (*this) * matrix;
	(*this) = tmp;

	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator+( const T &scalar )
{
	Matrix tmp( rows, columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = data[i][j] + scalar;

	return tmp;
}

template<typename T>
Matrix<T> & Matrix<T>::operator+=( const T &scalar )
{
	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			data[i][j] += scalar;

	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator-( const T &scalar )
{
	Matrix tmp( rows, columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = data[i][j] - scalar;

	return tmp;
}

template<typename T>
Matrix<T> & Matrix<T>::operator-=( const T &scalar )
{
	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			data[i][j] -= scalar;

	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator*( const T &scalar )
{
	Matrix tmp( rows, columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = data[i][j] * scalar;

	return tmp;
}

template<typename T>
Matrix<T> & Matrix<T>::operator*=( const T &scalar )
{
	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			data[i][j] *= scalar;

	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator/( const T &scalar )
{
	Matrix tmp( rows, columns );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp.data[i][j] = data[i][j] / scalar;

	return tmp;
}

template<typename T>
Matrix<T> & Matrix<T>::operator/=( const T &scalar )
{
	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			data[i][j] /= scalar;

	return *this;
}

template<typename T>
std::vector<T> Matrix<T>::operator*( const std::vector<T> &vector )
{
	std::vector<T> tmp( vector.size(), 0 );

	for( size_t i=0; i<rows; i++ )
		for( size_t j=0; j<columns; j++ )
			tmp[i] = data[i][j] * vector[j];

	return tmp;
}

template<typename T>
std::vector<std::vector<T>> Matrix<T>::minimum( const std::vector<std::vector<T>> &src, size_t I, size_t J, size_t ordSrc ) const
{
	std::vector<std::vector<T>> min( ordSrc-1, std::vector<T>( ordSrc-1, 0 ) );

	size_t rowCnt = 0;

	for( size_t i=0; i<ordSrc; i++ )
	{
		size_t colCnt = 0;

		if ( i != I )
		{
			for( size_t j=0; j<ordSrc; j++ )
				if ( j != J )
				{
					min[rowCnt][colCnt] = src[i][j];
					colCnt++;
				}

			rowCnt++;
		}
	}

	return min;
}

template<typename T>
T Matrix<T>::calculateDet( const std::vector<std::vector<T>> &src, size_t ord ) const
{
	T det = 0;

	if( 2 == ord )
		return src[0][0] * src[1][1] - src[0][1] * src[1][0];
	else
	{
		for (size_t i = 0; i < ord; i++)
		{
			std::vector<std::vector<T>> min = minimum(src, 0, i, ord);

			if (0 == i % 2)
				det += src[0][i] * calculateDet(min, ord - 1);
			else
				det -= src[0][i] * calculateDet(min, ord - 1);
		}

		return det;

	}
}
