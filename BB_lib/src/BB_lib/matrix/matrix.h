#ifndef BB_LIB_MATRIX_MATRIX_H_
#define BB_LIB_MATRIX_MATRIX_H_

#include <cstddef>
#include <vector>

template<typename T>
class Matrix
{
private:
	std::vector<std::vector<T>> data;
	size_t rows, columns;

	std::vector<std::vector<T>> minimum( const std::vector<std::vector<T>> &src, size_t I, size_t J, size_t ordSrc ) const;
	T calculateDet( const std::vector<std::vector<T>> &src, size_t ord ) const;

public:
	Matrix( size_t r, size_t c, const T &initVal = 0 );
	Matrix( const Matrix<T> &matrix );
	virtual ~Matrix();

	Matrix<T> transpose() const;
	Matrix<T> cofactor() const;
	Matrix<T> inverse() const;
	std::vector<T> diagonal() const;
	T determinant() const;
	size_t rank() const;

	T getElement( size_t r, size_t c ) const;
	void setElement( size_t r, size_t c, const T &value = 0 );
	size_t getRows() const;
	size_t getColumns() const;

	T & operator()( size_t r, size_t c );
	const T & operator()( size_t r, size_t c ) const;

	Matrix<T> & operator=( const Matrix<T> &matrix );
	Matrix<T> operator+( const Matrix<T> &matrix );
	Matrix<T> & operator+=( const Matrix<T> &matrix );
	Matrix<T> operator-( const Matrix<T> &matrix );
	Matrix<T> & operator-=( const Matrix<T> &matrix );
	Matrix<T> operator*( const Matrix<T> &matrix );
	Matrix<T> & operator*=( const Matrix<T> &matrix );

	Matrix<T> operator+( const T &scalar );
	Matrix<T> & operator+=( const T &scalar );
	Matrix<T> operator-( const T &scalar );
	Matrix<T> & operator-=( const T &scalar );
	Matrix<T> operator*( const T &scalar );
	Matrix<T> & operator*=( const T &scalar );
	Matrix<T> operator/( const T &scalar );
	Matrix<T> & operator/=( const T &scalar );

	std::vector<T> operator*( const std::vector<T> &vector );
};

#endif /* BB_LIB_MATRIX_MATRIX_H_ */
