#include <cstdlib>
#include <iostream>


namespace BB
{

typedef int32_t sizeType;

class string
{
private:
    // class data
    static const sizeType SIZE_TYPE_MAX_VALUE = 2147483647;
    static const sizeType CINLIM = 80;
    static sizeType amountOfstrings;

    // object data
    char *str;
    sizeType len;
public:
    // class functions
    static sizeType howMany();

    // member functions
    string();
    string(const string & s);
    string(const string & s, sizeType pos, sizeType n);
    string(const char * s);
    string(const char * s, sizeType pos, sizeType n);
    string(char c);
    string(sizeType n, char c);
    ~string();
    string & operator=(const string & s);
    string & operator=(const char * s);
    string & operator=(char c);
    string & assign (const string & s);
    string & assign (const string & s, sizeType subpos, sizeType sublen);
    string & assign (const char * s);
    string & assign (const char * s, sizeType n);
    string & assign (sizeType n, char c);

    // element access
    char & at( sizeType pos );
    const char & at( sizeType pos ) const;
    char & operator[](sizeType i);
    const char & operator[](sizeType i) const;
    char & front();
    const char & front() const;
    char & back();
    const char & back() const;
    char * data();
    const char * data() const;
    const char * c_str() const;

    // iterators
    /* something here*/

    // capacity
    bool empty() const;
    sizeType size() const;
    sizeType length() const;
    sizeType max_size() const;

    // operations
    void clear();
    string & insert (sizeType pos, const string & s);
    string & insert (sizeType pos, const string & s, sizeType subpos, sizeType sublen);
    string & insert (sizeType pos, const char * s);
    string & insert (sizeType pos, const char * s, sizeType n);
    string & insert (sizeType pos, sizeType n, char c);
    string & operator+=(const string & s);
    string & operator+=(const char * s);
    string & operator+=(char c);

    // search
    /* something here*/

    // sum of objects
    friend string operator+(const string & s1, const string & s2);
    friend string operator+(const string & s1, const char * s2);
    friend string operator+(const char * s1, const string & s2);
    friend string operator+(const string & s, char c);
    friend string operator+(char c, const string & s);

    // relational operators
    friend bool operator<(const string & s1, const string & s2);
    friend bool operator<=(const string & s1, const string & s2);
    friend bool operator>(const string & s1, const string & s2);
    friend bool operator>=(const string & s1, const string & s2);
    friend bool operator==(const string & s1, const string & s2);
    friend bool operator!=(const string & s1, const string & s2);

    // swap algorithm
    /* something here*/

    // input output streams
    friend std::ostream & operator<<(std::ostream & os, const string & s);
    friend std::istream & operator>>(std::istream & is, string & s);
};

}
