
#include "String.h"
#include <string.h>

namespace BB
{

/*--------------- class data start ---------------*/

sizeType string::amountOfstrings = 0;

/*--------------- class data end ---------------*/

/*--------------- class methods start ---------------*/

sizeType string::howMany()
{
    return amountOfstrings;
}

/*--------------- class methods end ---------------*/

/*--------------- object methods start ---------------*/

/********** member functions start **********/
string::string()
{
    len = 0;
    str = new char[1];
    str[0] = '\0';
    amountOfstrings++;
}

string::string(const string & s)
{
    len = s.len;
    str = new char[len+1];
    strcpy(str, s.str);
    amountOfstrings++;
}

string::string(const string & s, sizeType pos, sizeType n)
{
    sizeType offset;

    if(pos+n <= s.len)
    {
        len = n;
        offset = pos;
    }
    else if(pos <= s.len)
    {
        len = s.len - pos;
        offset = pos;
    }
    else
    {
       len = 0;
       offset = 0;
    }

    str = new char[len+1];
    strncpy(str, s.str+offset,len);
    amountOfstrings++;
}

string::string(const char * s)
{
    len = strlen(s);
    str = new char[len];
    strcpy(str, s);
    amountOfstrings++;
}

string::string(const char * s, sizeType pos, sizeType n)
{
    sizeType offset;
    sizeType length = strlen(s);;

    if(pos+n <= length)
    {
        len = n;
        offset = pos;
    }
    else if(pos <= length)
    {
        len = length - pos;
        offset = pos;
    }
    else
    {
       len = 0;
       offset = 0;
    }

    str = new char[len+1];
    strncpy(str, s+offset,len);
    amountOfstrings++;
}

string::string(char c)
{
    len = 1;
    str = new char[2];
    str[0] = c;
    str[1] = '\0';
    amountOfstrings++;
}

string::string(sizeType n, char c)
{
    len = n;
    str = new char[n+1];
    for(sizeType i=0; i<n; i++)
        str[i] = c;
    str[n] = '\0';
    amountOfstrings++;
}

string::~string()
{
    delete[] str;
    amountOfstrings--;
}

string & string::operator=(const string & s)
{
    if(&s == this)
    {
        return *this;
    }

    delete[] str;
    len = s.len;
    str = new char[len+1];
    strcpy(str, s.str);
    return *this;
}

string & string::operator=(const char * s)
{
    delete[] str;
    len = strlen(s);
    str = new char[len+1];
    strcpy(str, s);
    return *this;
}

string & string::operator=(char c)
{
    delete[] str;
    len = 1;
    str = new char[2];
    str[0] = c;
    str[1] = '\0';
    return *this;
}

string & string::assign (const string & s)
{

}

string & string::assign (const string & s, sizeType subpos, sizeType sublen)
{

}

string & string::assign (const char * s)
{

}

string & string::assign (const char * s, sizeType n)
{

}

string & string::assign (sizeType n, char c)
{

}
/********** member functions end **********/

/********** element access start **********/
char & string::at( sizeType pos )
{
    return str[pos];
}

const char & string::at( sizeType pos ) const
{
    return str[pos];
}

char & string::operator[](sizeType i)
{
    return str[i];
}

const char & string::operator[](sizeType i) const
{
    return str[i];
}

char & string::front()
{
    return str[0];
}

const char & string::front() const
{
    return str[0];
}

char & string::back()
{
    return str[len-1];
}

const char & string::back() const
{
    return str[len-1];
}

char * string::data()
{
    return str;
}

const char * string::data() const
{
    return str;
}

const char * string::c_str() const
{
    return str;
}
/********** element access end **********/

/********** iterators start **********/

/********** iterators end **********/

/********** capacity start **********/
bool string::empty() const
{
    return len==0?true:false;
}

sizeType string::size() const
{
    return len;
}

sizeType string::length() const
{
    return len;
}

sizeType string::max_size() const
{
    return SIZE_TYPE_MAX_VALUE;
}
/********** capacity end **********/

/********** operations start **********/
void string::clear()
{
    delete[] str;
    len = 0;
    str = new char[1];
    str[0] = '\0';
}

string & string::insert (sizeType pos, const string & s)
{

}

string & string::insert (sizeType pos, const string & s, sizeType subpos, sizeType sublen)
{

}

string & string::insert (sizeType pos, const char * s)
{

}

string & string::insert (sizeType pos, const char * s, sizeType n)
{

}

string & string::insert (sizeType pos, sizeType n, char c)
{

}

string & string::operator+=(const string & s)
{
    return *this;
}

string & string::operator+=(const char * s)
{
    return *this;
}

string & string::operator+=(char c)
{
    return *this;
}
/********** operations end **********/

/********** search start **********/

/********** search end **********/

/********** sum of objects start **********/
string operator+(const string & s1, const string & s2)
{
    char *tmpStr = new char[s1.len+s2.len+1];
    strcpy(tmpStr, s1.str);
    strcat(tmpStr, s2.str);
    string tmp(tmpStr);
    delete[] tmpStr;
    return tmp;
}

string operator+(const string & s1, const char * s2)
{
    char *tmpStr = new char[s1.len+strlen(s2)+1];
    strcpy(tmpStr, s1.str);
    strcat(tmpStr, s2);
    string tmp(tmpStr);
    delete[] tmpStr;
    return tmp;
}

string operator+(const char * s1, const string & s2)
{
    char *tmpStr = new char[strlen(s1)+s2.len+1];
    strcpy(tmpStr, s1);
    strcat(tmpStr, s2.str);
    string tmp(tmpStr);
    delete[] tmpStr;
    return tmp;
}

string operator+(const string & s, char c)
{
    char *tmpStr = new char[s.len+2];
    strcpy(tmpStr, s.str);
    tmpStr[s.len+1] = c;
    tmpStr[s.len+2] = '\0';
    string tmp(tmpStr);
    delete[] tmpStr;
    return tmp;
}

string operator+(char c, const string & s)
{
    char *tmpStr = new char[s.len+2];
    tmpStr[0] = c;
    strcpy(tmpStr+1, s.str);
    string tmp(tmpStr);
    delete[] tmpStr;
    return tmp;
}
/********** sum of objects end **********/

/********** relational operators start **********/
bool operator<(const string & s1, const string & s2)
{
    return (strcmp(s1.str, s2.str) < 0);
}

bool operator<=(const string & s1, const string & s2)
{
    return (strcmp(s1.str, s2.str) <= 0);
}

bool operator>(const string & s1, const string & s2)
{
    return (strcmp(s1.str, s2.str) > 0);
}

bool operator>=(const string & s1, const string & s2)
{
    return (strcmp(s1.str, s2.str) >= 0);
}

bool operator==(const string & s1, const string & s2)
{
    return (strcmp(s1.str, s2.str) == 0);
}

bool operator!=(const string & s1, const string & s2)
{
    return (strcmp(s1.str, s2.str) != 0);
}
/********** relational operators end **********/

/********** swap algorithm start **********/

/********** swap algorithm end **********/

/********** input output streams start **********/
std::ostream & operator<<(std::ostream & os, const string & s)
{
    os << s.str;
    return os;
}

std::istream & operator>>(std::istream & is, string & s)
{
    char temp[string::CINLIM];
    is.get(temp, string::CINLIM);
    if (is)
        s = temp;
    while (is && is.get() != '\n')
        continue;
    return is;
}
/********** input output streams end **********/

/*--------------- object methods end ---------------*/

}
