/*
 * fifo.h
 *
 *  Created on: Jul 18, 2018
 *      Author: BloedeBleidd
 */

#ifndef BB_LIB_FIFO_FIFO_H_
#define BB_LIB_FIFO_FIFO_H_

#include <iostream>

namespace BB
{

template <typename T> class Fifo
{
	struct Node	{ T value; Node *next; };
	Node *head = nullptr, *tail = nullptr;
	size_t n = 0;

public:
	bool empty() const	{ return !n; }
	size_t size() const	{ return n; }

	void enqueue( const T &element )
	{
		Node *tmp = new Node;
		tmp->value = element;
		tmp->next = nullptr;

		if( empty() )
			head = tail = tmp;
		else
		{
			tail->next = tmp;
			tail = tmp;
		}

		n++;
	}

	bool dequeue( T &element )
	{
		if( empty() )
			return false;

		element = head->value;
		Node *tmp = head;
		head = head->next;
		delete tmp;
		n--;

		return true;
	}
};

}

#endif /* BB_LIB_FIFO_FIFO_H_ */
