/*
 * crc.h
 *
 *  Created on: Feb 13, 2018
 *      Author: Y520
 */

#ifndef CRC_H_
#define CRC_H_

#include <iostream>

namespace BB
{

struct CRC_DATA
{
	uint32_t polly;
	uint8_t lenght;
	uint32_t initialVal;
	uint32_t finalXor;
	bool inputReflected;
	bool resultReflected;
};

class Crc
{
public:
	enum CRC_TYPE
	{
		NONE = -2, CUSTOM, PARITY,
		CRC8, CRC8_WCDMA, CRC8_SAE_J1850, CRC8_SAE_J1850_ZERO, CRC8_8H2F, CRC8_CDMA2000, CRC8_DARC, CRC8_DVB_S2, CRC8_EBU, CRC8_ICODE, CRC8_ITU, CRC8_MAXIM, CRC8_ROHC,
		CRC16_MODBUS, CRC16_USB, CRC16_A, CRC16_CCIT_ZERO, CRC16_CCITT_FALSE, CRC16_AUG_CCITT, CRC16_ARC, CRC16_BUYPASS, CRC16_CDMA2000, CRC16_DDS_110, CRC16_DECT_R, CRC16_DECT_X,
		CRC16_DNP, CRC16_EN_13757, CRC16_GENIBUS, CRC16_MAXIM, CRC16_MCRF4XX, CRC16_RIELLO, CRC16_T10_DIF, CRC16_TELEDISK, CRC16_TMS37157, CRC16_KERMIT, CRC16_X_25, CRC16_XMODEM,
		CRC32, CRC32_BZIP2, CRC32_MPEG2, CRC32_POSIX, CRC32_JAMCRC, CRC32_XFER, CRC32_C, CRC32_D ,CRC32_Q
	};

	enum CRC_FORMAT { BINARY = 2, OCTAL = 8, DECIMAL = 10, HEX = 16 };

private:
	static const CRC_DATA CRC_TAB[];
	static const int TAB_SIZE = 256;
	CRC_DATA custom = { 0, 0, 0, 0, false, false };

	uint32_t tab[TAB_SIZE];
	CRC_TYPE currentType = NONE;
	CRC_FORMAT resultFormat = HEX;

	uint32_t swapBits( uint32_t data, int lenght ) const;
	uint32_t mask( uint32_t data, CRC_TYPE type ) const;
	bool createLookupTable( CRC_TYPE type );
	int calculateCRC( const char *s, const CRC_DATA &crcData ) const;
	CRC_DATA checkIfCustom() const;

public:
	Crc( CRC_TYPE type );
	Crc( const CRC_DATA &custom );

	uint32_t calculate( const char *s ) const;
	uint32_t calculate( const char *s, char *result ) const;

	void addCustomCrc( const CRC_DATA &custom );

	bool setType( CRC_TYPE type );
	void setResultFormat( CRC_FORMAT format ) { resultFormat = format; };

	CRC_TYPE getCurrentType() const { return currentType; }
	CRC_FORMAT getCrcResultFormat() const { return resultFormat; }
};

}


#endif /* CRC_H_ */
