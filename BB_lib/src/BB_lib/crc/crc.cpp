/*
 * crc.cpp
 *
 *  Created on: Feb 13, 2018
 *      Author: Y520
 */


#include "crc.h"
#include <cstring>

namespace BB
{

const CRC_DATA Crc::CRC_TAB[] =
{
	{ 0x1, 1, 0x0, 0x0, false, false },							//PARITY
	{ 0x7, 8, 0x0, 0x0, false, false },							//CRC8
	{ 0x9b, 8, 0x0, 0x0, true, true },							//CRC8_WCDMA
	{ 0x1d, 8, 0xff, 0xff, false, false },						//CRC8_SAE_J1850
	{ 0x1d, 8, 0x0, 0x0, false, false },						//CRC8_SAE_J1850_ZERO
	{ 0x2f, 8, 0xff, 0xff, false, false },						//CRC8_8H2F
	{ 0x9b, 8, 0xff, 0x0, false, false },						//CRC8_CDMA2000
	{ 0x39, 8, 0x0, 0x0, true, true },							//CRC8_DARC
	{ 0xd5, 8, 0x0, 0x0, false, false },						//CRC8_DVB_S2
	{ 0x1d, 8, 0xff, 0x0, true, true },							//CRC8_EBU
	{ 0x1d, 8, 0xfd, 0x0, false, false },						//CRC8_ICODE
	{ 0x7, 8, 0x0, 0x55, false, false },						//CRC8_ITU
	{ 0x31, 8, 0x0, 0x0, true, true },							//CRC8_MAXIM
	{ 0x7, 8, 0xff, 0x0, true, true },							//CRC8_ROHC
	{ 0x8005, 16, 0xffff, 0x0, true, true },					//CRC16_MODBUS
	{ 0x8005, 16, 0xffff, 0xffff, true, true },					//CRC16_USB
	{ 0x1021, 16, 0xc6c6, 0x0, true, true },					//CRC16_A
	{ 0x1021, 16, 0x0, 0x0, false, false },						//CRC16_CCIT_ZERO
	{ 0x1021, 16, 0xffff, 0x0, false, false },					//CRC16_CCITT_FALSE
	{ 0x1021, 16, 0x1d0f, 0x0, false, false },					//CRC16_AUG_CCITT
	{ 0x8005, 16, 0x0, 0x0, true, true },						//CRC16_ARC
	{ 0x8005, 16, 0x0, 0x0, false, false },						//CRC16_BUYPASS
	{ 0xc867, 16, 0xffff, 0x0, false, false },					//CRC16_CDMA2000
	{ 0x8005, 16, 0x800d, 0x0, false, false },					//CRC16_DDS_110
	{ 0x589, 16, 0x0, 0x1, false, false },						//CRC16_DECT_R
	{ 0x589, 16, 0x0, 0x0, false, false },						//CRC16_DECT_X
	{ 0x3d65, 16, 0x0, 0xffff, true, true },					//CRC16_DNP
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_EN_13757
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_GENIBUS
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_MAXIM
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_MCRF4XX
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_RIELLO
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_T10_DIF
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_TELEDISK
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_TMS37157
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_KERMIT
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_X_25
	{ 0x3d65, 16, 0x0, 0x0, true, true },						//CRC16_XMODEM
	{ 0x04c11db7, 32, 0xffffffff, 0xffffffff, true, true },		//CRC32
	{ 0x04c11db7, 32, 0xffffffff, 0xffffffff, false, false },	//CRC32_BZIP
	{ 0x04c11db7, 32, 0xffffffff, 0x0, false, false },			//CRC32_MPEG2
	{ 0x04c11db7, 32, 0x0, 0xffffffff, false, false },			//CRC32_POSIX
	{ 0x04c11db7, 32, 0xffffffff, 0x0, true, true },			//CRC32_JAMCRC
	{ 0xaf, 32, 0x0, 0x0, false, false },						//CRC32_XFER
	{ 0x1edc6f41, 32, 0xffffffff, 0xffffffff, true, true },		//CRC32_C
	{ 0xa833982b, 32, 0xffffffff, 0xffffffff, true, true },		//CRC32_D
	{ 0x814141ab, 32, 0x0, 0x0, false, false },					//CRC32_Q
};

Crc::Crc( CRC_TYPE type )
{
	createLookupTable( type );
}

Crc::Crc( const CRC_DATA &custom )
{
	addCustomCrc( custom );
	createLookupTable( CUSTOM );
}

uint32_t Crc::swapBits( uint32_t data, int lenght ) const
{
	uint32_t result = 0;
	for( int i=0; i<lenght-1; i++, data >>= 1, result <<= 1 )
		if( data & 1 )
			result |= data & 1;
	return result;
}

uint32_t Crc::mask( uint32_t data, CRC_TYPE type ) const
{
	return ( data & ( 0xffffffff >> ( 32 - CRC_TAB[type].lenght ) ) );
}

bool Crc::createLookupTable( CRC_TYPE type )
{
	if( type == currentType )
		return false;

	currentType = type;
	uint32_t tmp;
	CRC_DATA tmpCrc;

	if( currentType == CUSTOM )
		tmpCrc = custom;
	else
		tmpCrc = CRC_TAB[currentType];

	for( int i=0; i<TAB_SIZE ; i++ )
	{
		tmp = i << ( tmpCrc.lenght - 8 );

		for ( int j=0; j<8; j++ )
		{
			if( ( tmp & ( 1 << ( tmpCrc.lenght - 1 ) ) ) != 0 )
			{
				tmp <<= 1;
				tmp = mask( tmp, type );
				tmp ^= tmpCrc.polly;
			}
			else
			{
				tmp <<= 1;
				tmp = mask( tmp, type );
			}
		}

		tab[i] = mask( tmp, type );
	}

	return true;
}

int Crc::calculateCRC( const char *s, const CRC_DATA &crcData ) const
{
	int lenght = strlen( s );
	int32_t crc = crcData.initialVal;
	int shift = crcData.lenght - 8;

	for( int i=0; i<lenght; i++, s++ )
	{
		uint8_t pos = ( crc ^ ( ( ( crcData.inputReflected == true ) ? ( swapBits( *s, 8 ) ) : *s ) << shift ) ) >> shift;
		crc = crc << 8 ;
		crc = mask( crc, currentType );
		crc ^= tab[ pos ];
	}

	crc = ( crcData.resultReflected == true ) ? ( swapBits( crc, crcData.lenght ) ) : crc;
	crc ^= crcData.finalXor;
	return crc;
}

CRC_DATA Crc::checkIfCustom() const
{
	if( currentType == CUSTOM )
		return custom;
	else
		return CRC_TAB[currentType];
}

uint32_t Crc::calculate( const char *s ) const
{
	return calculateCRC( s, checkIfCustom() );
}

uint32_t Crc::calculate( const char *s, char *result ) const
{
	uint32_t crc = calculate( s );
	itoa( crc, result, resultFormat );
	return crc;
}

void Crc::addCustomCrc( const CRC_DATA &custom )
{
	Crc::custom = custom;
}

bool Crc::setType( CRC_TYPE type )
{
	return createLookupTable( type );
}

}



